from jitcdde import jitcdde, y, dy, t
import numpy as np
import matplotlib
matplotlib.use("Qt5Agg")
import matplotlib.pyplot as plt
import scipy.sparse.linalg as lin
import scipy.linalg as linal
from scipy import integrate as integ

#the parameters tau and alpha from the Suarez-Schopf model

tau = 0.83
alpha = 0.6

#computing the radius of a ball enclosing the global attractor via the lemma from the paper
CP = (alpha * tau)/(1-alpha*tau) * (4./3.) * (1 - alpha) * np.sqrt((1. - alpha)/3.)
r = np.roots([-1,0,1-alpha,CP])

print(r)

r = np.real(r[0])

#the coefficient Lambda
Lam = 0.5 * (3 * r * r)

LamM = 1./Lam

#time steps at which solution is obtained
step = 0.001

#Tolerance for numerical integration
TOL = 1e-6

delay_in_steps = int(tau / step)


#max step in time for integration
maxstep = 1e-5

#the time upto which solutions are obtained
finish = 15.
#the length of array corresponding to the time interval [0,T] according to step
Tstep = int(finish/step)

PsiINF0 = np.sqrt(2) #(-1)**(m+1) * (m!)**0.5 * Btilde
#initial condition \psi_{\infty}
def histDelta0(t):
    return PsiINF0 * np.heaviside(t,1)

#maximum number of trigonometric basis vectors to compute; below one may use different N from the approximation scheme such that N <= maxN
maxN=10

#vector field for the linear equation to integrate
fLin = {
    y(i, t): (1.-Lam) * y(i, t) - alpha * y(i, t - tau)

    for i in range(1)
}

def get_solution(DDE, history):
    DDE.past_from_function(history)
    DDE.set_integration_parameters(max_step=maxstep, first_step=maxstep, atol=TOL, rtol=TOL)
    DDE.adjust_diff()

    ss = []
    ts = []
    for time in np.arange(step, finish, step):
        ts.append(time)
        state = DDE.integrate(time)
        ss.append(state)
    ss = np.array(ss)
    return ts, ss

tt = np.arange(0, finish, step=step)

DDE = jitcdde(fLin, max_delay=tau)

#computing x_{\infty}
sINF, ssINF = get_solution(DDE=DDE, history=histDelta0)
yyINF = []
for i in reversed(range(0,delay_in_steps)):
    yyINF.append(histDelta0(-i*step))
yyINF.extend(ssINF[:,0])
yyINF = np.array(yyINF)

#if we want to print the graph of x_{\infty}
#plt.plot(np.arange(-1, finish-step, step=step, dtype=np.float64), yyINF)
#plt.tick_params(axis='both', which='major', labelsize=20)
#
#plt.grid()
#plt.show()

xAR = []

for k in range(0,maxN+1):
    def histOrtKR(t):
        return 1./(tau**0.5) *np.cos(k*2*np.pi*t/tau)

    def histOrtKIM(t):
        return 1./(tau**0.5) *np.sin(k*2*np.pi*t/tau)

    s11, ss11 = get_solution(DDE=DDE, history=histOrtKR)
    s12, ss12 = get_solution(DDE=DDE, history=histOrtKIM)
    yy11 = []
    for i in reversed(range(0,delay_in_steps)):
        yy11.append(histOrtKR(-i*step))
    yy11.extend(ss11[:,0])
    yy11 = np.array(yy11)
    yy12 = []
    for i in reversed(range(0,delay_in_steps)):
        yy12.append(histOrtKIM(-i*step))
    yy12.extend(ss12[:,0])
    yy12 = np.array(yy12)

#if we want to print graphs for some solutions
#    plt.plot(np.arange(-1, finish-step, step=step, dtype=np.float64), yy11)
#    plt.plot(np.arange(-1, finish-step, step=step, dtype=np.float64), yy12)
#    plt.tick_params(axis='both', which='major', labelsize=20)
#
#    plt.grid()
#    plt.show()

    xAR.append(yy11 + 1j*yy12)

    #above we have added x_{k}; now we add x_{-k}
    if k > 0:
        xAR.append(yy11 - 1j*yy12)

    print('X[', k, '] computed')

#making an array of xAR; so xAR = [x_{0}, x_{1}, x_{-1}, x_{2}, x_{-2}, ...]
xAR = np.array(xAR)

print('Solutions computed')

theta = np.array([-tau + step * i for i in range(delay_in_steps)])
basisConj_dict = {}

for j in range(-maxN, maxN+1):
    #define sqrt(m) * (U^{1}_{j})*; the coefficient sqrt(m) is taken into account at the end of calc_matrix(p)
    def ConjOrtBase(t):
        return np.exp(-1j * j * 2 * np.pi * t / tau) / (np.sqrt(tau))
    basisConj_dict[(j)] = ConjOrtBase(theta)

print('Basis vectors computed')

#calculating matrix W_{T,N}(p)
def calc_matrix(p):
    M = np.empty((2*N+1, 2*N+1),dtype=complex)
    print('Start computation for p=',p)
    theta = np.array([-tau + step * i for i in range(delay_in_steps)])
    Tinterval = np.array([0 + step * i for i in range(Tstep)])
    aExp = np.exp(-p * Tinterval)
    for k in range(-N, N+1):
        counter = -1
        
        YgK = []
        for s in theta:
             counter = counter + 1
             if k > 0:
                num = 2*k-1
             else:
                num = -2*k
             ans = np.array(xAR[num])
             intGt = 0.5 * ( ans[delay_in_steps-1 : Tstep + delay_in_steps] * yyINF[counter : Tstep + counter] - ans[counter : Tstep + counter] * yyINF[delay_in_steps-1 : Tstep + delay_in_steps] )
             y = aExp * intGt
             YgK.append(integ.simpson(y,Tinterval))
        YgK = np.array(YgK)
        for j in range(-N, N+1):
            M[j+N,k+N] = np.sqrt(2) * integ.simpson( YgK * basisConj_dict[(j)],theta) # sqrt(2) = m / sqrt(m), where m from (AS.3.3) and 1/sqrt(m) from the definition of U^{1}_{k}
    print('Matrix computed')
    return M

def calc_freq(p):
    M = calc_matrix(p)
    svdM = linal.svd(M, compute_uv=False)
    w = svdM[0]
    print('Largest singular value computed: ', w)
    print('Cond number: ', svdM[0]/svdM[2*N])
    w = np.real(w)
    return w

def run_rho(rho,omegas):
    w = []
    for omega in omegas:
        w.append(calc_freq(1j * omega + rho))
    return np.array(w)

#Omega from the aproximation scheme determining the interval [-Omega, Omega]
OmegaBIG = 5.

#Step for omega in the inverval [-Omega, Omega]
omega_step = 0.25
omegas = np.arange(-OmegaBIG, OmegaBIG, step=omega_step, dtype=np.float64)

#the parameter nu0 from the frequency condition; to justify the stability we need nu0 > 0
nu0 = 0.01

rho = -nu0

#start computation of alpha_{T,N}(-nu0 + i*omega) for omega in [-Omega, Omega]
N = 10
w1 = run_rho(rho,omegas)

#uncomment and change maxN above to a proper value if you want to compute the graphs for different N <= maxN

#N = 20
#print('Start computation for N=20')
#w2 = run_rho(rho,omegas)
#N = 30
#print('Start computation for N=30')
#w3 = run_rho(rho,omegas)

#plotting the graphs of alpha_{T,N}(-nu0 + i*omega) versus omega

fig, ax = plt.subplots(1, figsize=(4, 4))
plt.rcParams['text.usetex'] = True

plt.plot(omegas, w1)
plt.plot(omegas, [LamM]*len(omegas))
#plt.plot(omegas, w2)
#plt.plot(omegas, w3)
ax.set_xlabel("omega", fontsize=20)
ax.set_ylabel("alpha", fontsize=20)
plt.tick_params(axis='both', which='major', labelsize=20)

plt.grid()

plt.show()
